/*==========================================================================

  Author: Li, Xiaozhou

  Email: guzhou@mail.ustc.edu.cn or xiaozhouli@live.com

  Creat Time:  2010-11-13
 
  Last Change: 2010-11-13
 
  File Name:   24_Game.cpp
 
  Description: 
	The 24 Game is a mathematical card game in which the object is to find a way to manipulate four integers so that the end result is 24. Some operations like addition, substraction, multiplication and division, may be used to make four integers equal 24.
    This program requires the input of four positive integers, and output a possible solution (or No solution).
=========================================================================*/
#include <iostream> 

using namespace std;

typedef double ( *TYPE_MYFUN)(double , double ); 

double FunAdd(double x, double y) 
{
	return x + y; 
} 

double FunSub(double x, double y) 
{
	return x-y; 
} 

double FunMul(double x, double y) 
{
	return x * y; 
}

double FunDiv(double x, double y)
{
	return x / y; 
} 

bool operatorFun(double a,double b,double c,double d); 

int main(int argc, char* argv[]) 
{ 
	double Num[4] = {0};
	for( int i = 0; i < 4; i++)
		cin >> Num[i];
	for( int a = 0 ; a < 4 ; a++ )
	{ 
		for( int b = 0 ; b < 4 ;b++ )
		{
			for( int c = 0 ; c < 4 ; c++ )
			{ 
				for( int d = 0 ; d < 4 ; d++ )
				{ 
					if((a==b)||(a==c)||(a==d)||(b==c)||(b==d)||(c==d)) 
						continue; 
					if ( operatorFun(Num[a],Num[b],Num[c],Num[d]) ) 
						return 0; 
				} 
			} 
		} 
	} 
	cout << "No solution" << endl;
	return 0; 
} 

bool operatorFun( double a, double b ,double c ,double d) 
{ 
	static TYPE_MYFUN Fun[4] = { FunAdd ,FunSub , FunMul , FunDiv }; 
	static char op[4] = { '+' , '-' , '*' ,'/'}; 
	for( int x = 0 ; x < 4 ; x++ )
	{ 
		for( int y = 0 ; y < 4 ; y++ )
		{
			for( int z = 0 ; z < 4 ; z++ )
			{
				double sum = Fun[z]( Fun[y]( Fun[x](a,b) ,c) ,d); 
				if( (sum >23.9)&&(sum<24.1) )
				{
					cout << "((" << (int)a << op[x] << (int)b << ")" << op[y] << (int)c << ")" << op[z] << (int)d << endl;
					return true; 
				}
				sum = Fun[z]( Fun[x](a,b) , Fun[y](c ,d) ); 
				if( (sum >23.9)&&(sum<24.1) )
				{
					cout << "(" << (int)a << op[x] << (int)b << ")" << op[z] << "(" << (int)c << op[y] << (int)d << endl;
					return true; 
				} 
				sum = Fun[z]( a , Fun[y](b , Fun[x](c,d) ) ); 
				if( (sum >23.9)&&(sum<24.1) )
				{ 
					cout << (int)a << op[z] << "(" << (int)b << op[y] << "(" << (int)c << op[x] << (int)d << "))" << endl; 
					return true; 
				} 
			} 
		} 
	}
	return false; 
}
